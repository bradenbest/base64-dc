CFLAGS = -O3 -Wall -Wextra

all: bytestream

bytestream: bytestream.c

clean:
	rm -f bytestream

.PHONY: clean all
