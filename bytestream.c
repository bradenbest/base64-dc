/* Reads input out as series of space-separated bytes in decimal
 * the first line is the length of the input in bytes, then the rest of
 * the lines contain bytes encoded in decimal, space-separated, argv[1]
 * bytes (default 10) per line.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node {
    unsigned char const *  buffer;
    size_t        length;
    struct node * next;
};

static size_t nread_total;
static struct node *head;
static struct node *tail;
static int bytes_per_line = 10;

static struct node *  collapse_bytestream (void);
static int            print_bytestream    (void);
static int            push_node           (unsigned char const *buffer, size_t length);
static void           free_nodes          (void);

static struct node *
collapse_bytestream(void)
{
    struct node *new = malloc(sizeof *new);
    unsigned char *bufcpy = malloc(nread_total);
    size_t full_length = 0;
    struct node *selnode = head;

    if (new == NULL || bufcpy == NULL)
        return NULL;

    while (selnode != NULL) {
        memcpy(bufcpy + full_length, selnode->buffer, selnode->length);
        full_length += selnode->length;
        selnode = selnode->next;
    }

    free_nodes();
    *new = (struct node){ bufcpy, full_length, NULL };
    return new;
}

static int
print_bytestream(void)
{
    struct node *full_text = collapse_bytestream();

    if (full_text == NULL)
        return 0;

    printf("%lu\n", full_text->length);

    for (size_t curbyte = 0; curbyte < full_text->length; ++curbyte) {
        int bytes_this_line = curbyte % bytes_per_line;

        printf("%03u ", full_text->buffer[curbyte]);

        if (bytes_this_line == bytes_per_line - 1)
            fwrite("\n", 1, 1, stdout);
    }

    free((unsigned char *)full_text->buffer);
    free(full_text);

    return 1;

}

static int
push_node(unsigned char const *buffer, size_t length)
{
    struct node *new = malloc(sizeof *new);
    unsigned char *bufcpy = malloc(sizeof *bufcpy * length);

    if (new == NULL || bufcpy == NULL)
        return 0;

    memcpy(bufcpy, buffer, length);
    *new = (struct node){ bufcpy, length, NULL };
    nread_total += length;

    if (head == NULL)
        head = tail = new;

    else {
        tail->next = new;
        tail = new;
    }

    return 1;
}

static void
free_nodes(void)
{
    struct node *selnode = head;
    struct node *next;

    while (selnode != NULL) {
        next = selnode->next;
        free((unsigned char *)selnode->buffer);
        free(selnode);
        selnode = next;
    }
}

int
main(int argc, char **argv)
{
    unsigned char buffer[4096];
    size_t nread;

    if (argc > 1)
        bytes_per_line = atoi(argv[1]);

    while ((nread = fread(buffer, sizeof *buffer, 4096, stdin)) > 0)
        if (!push_node(buffer, nread))
            return 1;

    if (!print_bytestream())
        return 1;

    return 0;
}
