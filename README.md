# Base64 in Unix DC

Base64 encoder and decoder written in dc (and a little bit of C) by Braden Best

## Usage

To use, first run `make` to compile the bytestream program

    $ make
    cc -O3 -Wall -Wextra    bytestream.c   -o bytestream
    $ dc binary-palette.dc > palette
    $ ./base64-enc.sh palette > palette.b64
    $ cat palette.b64
    AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS...
    $ ./base64-dec.sh palette.b64 > palette2
    $ md5sum palette palette2
    e2c865db4162bed963bfaa9ef6ac18f0  palette
    e2c865db4162bed963bfaa9ef6ac18f0  palette2

## What?

dc is a very old Unix program that appeared circa 1969-1972. It is one of the oldest Unix programs, predating even the
assembler and the invention of the C Programming language.

dc, short for "desk calculator", is a simple reverse polish stack-based arbitrary-precision calculator which is pretty
well-described in its manpage. Beyond being a calculator, it is a whole esoteric programming language and it is turing
complete. Because it is turing complete, that means it is capable of running complicated tasks, like approximating e,
or in this case, implementing a working base64 encoder and decoder.

The algorithm used is actually quite simple, just some lookup tables and comparisons, but because dc has some
limitations, the design is made complicated.

### dc cannot take input in the way that conventional programming languages can.

The only method of taking input that dc provides is the `?` command, which takes a line of stdin and runs it as a dc
macro. Perfect for taking space-separated numbers as input. Horrible for taking strings. To take a string as input, you
have to wrap it in `[]`, and even then, taking non-printing characters like 0x00 of 0x7F is complicated if not
impossible

### dc cannot convert a character to a character code

dc strings are 100% immutable and there is no such functionality to convert a one-character string like 'A' to its ASCII
codepoint (65). You can make "pseudo strings" by making very large numbers like 0x4142434445464748494A. The `P` command
will interpret these numbers as a string of bytes and print them out as such, so `16i 4142434445464748494A P` would
print out "ABCDEFGHIJ" with no newline.

### dc lacks any bitwise operators

Because dc numbers are bignums, dc has no need for bitwise operators like AND, OR, XOR and SHIFT.

## So how is this possible, then?

While dc lacks bitwise operators, the bitwise operations required to pack 3 8-bit numbers into 4 6-bit numbers (and vice
versa) can be approximated using multiplication, division and modulus, which are all operations that dc has.

As for the I/O and string manipulation features that dc lacks, I had to write a small C program to take input from stdin
and format it in a way that dc can use. This program is called `bytestream`, and it's included in this package.

bytestream is a simple program that buffers all the input it takes from stdin and then prints out the length of the
input on the first line, followed by the input itself encoded as space-separated base 10 numbers. By default, it emits
10 per line, but also it takes an argument to configure this per-line amount.

    $ ./bytestream 4 <<< "Hello"
    6
    072 101 108 108
    111 010

dc programs can then take this input and work with strings.

I don't see a problem with this, as C programs also can't natively access from the hard drive. You need an OS and
filesystem for libc's I/O functions to wrap. This program plays a similar role.

"Turing complete" doesn't mean "can do anything". It means that it can compute anything that a turing machine can
compute. Brainfuck would still be turing complete if, for example, it lacked the `.` and `,` commands. It would be
impossible to do I/O with it, but I/O is not a requirement for turing completeness.
